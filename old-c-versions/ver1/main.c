#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<string.h>
#include<math.h>

/*
	0: Roman
	1: Ascii
	2: Binary
	|
	V
   16: Hexadecimal
*/

int   Menu_Output(const char str1[9], const char str2[8], const char str3[8], const char str4[8], const char str5[5], const char title[18]);
int   Convert2Dec (char numb_1[9], int base_1);
char* Convert2Base(int  numb_dec,  int base_2);

int main() {
	int i = 0;
	//Numbers
	static char numb_1[17] = { 0 }, numb_2[65] = { 0 };
	if (numb_1[0] == '\0' && numb_2[0] == '\0') {
		strcpy(numb_1, "[Number]");
		strcpy(numb_2, "[Result]");
	}
	//Bases
	static int  base_1 = 16, base_2 = 0;
	static char base_1_str[8] = { 0 }, base_2_str[8] = { 0 };
	switch (base_1) {
	case 0:
		strcpy(base_1_str, "Roman");
		break;
	case 1:
		strcpy(base_1_str, "Ascii");
		break;
	case  2:
		strcpy(base_1_str, "Base 02");
		break;
	case  3:
		strcpy(base_1_str, "Base 03");
		break;
	case  4:
		strcpy(base_1_str, "Base 04");
		break;
	case  5:
		strcpy(base_1_str, "Base 05");
		break;
	case  6:
		strcpy(base_1_str, "Base 06");
		break;
	case  7:
		strcpy(base_1_str, "Base 07");
		break;
	case  8:
		strcpy(base_1_str, "Base 08");
		break;
	case  9:
		strcpy(base_1_str, "Base 09");
		break;
	case 10:
		strcpy(base_1_str, "Base 10");
		break;
	case 11:
		strcpy(base_1_str, "Base 11");
		break;
	case 12:
		strcpy(base_1_str, "Base 12");
		break;
	case 13:
		strcpy(base_1_str, "Base 13");
		break;
	case 14:
		strcpy(base_1_str, "Base 14");
		break;
	case 15:
		strcpy(base_1_str, "Base 15");
		break;
	case 16:
		strcpy(base_1_str, "Base 16");
		break;
	}
	switch (base_2) {
	case 0:
		strcpy(base_2_str, "Roman");
		break;
	case 1:
		strcpy(base_2_str, "Ascii");
		break;
	case  2:
		strcpy(base_2_str, "Base 02");
		break;
	case  3:
		strcpy(base_2_str, "Base 03");
		break;
	case  4:
		strcpy(base_2_str, "Base 04");
		break;
	case  5:
		strcpy(base_2_str, "Base 05");
		break;
	case  6:
		strcpy(base_2_str, "Base 06");
		break;
	case  7:
		strcpy(base_2_str, "Base 07");
		break;
	case  8:
		strcpy(base_2_str, "Base 08");
		break;
	case  9:
		strcpy(base_2_str, "Base 09");
		break;
	case 10:
		strcpy(base_2_str, "Base 10");
		break;
	case 11:
		strcpy(base_2_str, "Base 11");
		break;
	case 12:
		strcpy(base_2_str, "Base 12");
		break;
	case 13:
		strcpy(base_2_str, "Base 13");
		break;
	case 14:
		strcpy(base_2_str, "Base 14");
		break;
	case 15:
		strcpy(base_2_str, "Base 15");
		break;
	case 16:
		strcpy(base_2_str, "Base 16");
		break;
	}
	//Process
	switch (Menu_Output(numb_1, base_1_str, base_2_str, numb_2, "Exit", "UltimateConverter")) {
		//LEFT
		case -1:
			base_1--;
			break;
		case -2:
			base_2--;
			break;
		//RIGHT
		case 1:
			base_1++;
			break;
		case 2:
			base_2++;
			break;
		//SET
		case 5:
			system("cls");
			printf("    UltimateConverter\n    ");
			for (i = 0; i < 17; i++) {
				printf("%c", 205);
			}
			printf("\n\n");
			printf(" > [              ]\r > [");
			fgets(numb_1, sizeof(numb_1), stdin);
			numb_1[strlen(numb_1)-1] = '\0';
			break;
		case 25:
			exit(EXIT_SUCCESS);
			break;
	}
	//�bertrag
	base_1 > 16 ? base_1 = 0 : base_1;
	base_1 < 0 ? base_1 = 16 : base_1;
	base_2 > 16 ? base_2 = 0 : base_2;
	base_2 < 0 ? base_2 = 16 : base_2;
	//Umwandlung
	if (strcmp(numb_1, "[Number]")) strcpy(numb_2, Convert2Base(Convert2Dec(numb_1, base_1), base_2));

	return(main());
}

int Menu_Output(const char str1[9], const char str2[8], const char str3[8], const char str4[8], const char str5[5], const char title[18]) {
	static int row = 5;
	int chose, i, len = strlen(title);
	char selec[5] = { ' ', ' ', ' ', ' ', ' ' };
	while (1) {
				//TITLE
		system("cls");
		printf("    %s\n    ", title);
		for (i = 0; i < len; i++) {
			printf("%c", 205);
		}
		printf("\n\n");
				//MENU
		//�bertrag
		row < 0 ? row = 4 : row;
		row > 4 ? row = 0 : row;
		//Set
		selec[row] = '>';
		//Output
		printf(" %c  %s \n",   selec[0], str1);
		printf(" %c %c%s%c\n", selec[1], selec[1] == '>' ? '<' : ' ', str2, selec[1]);
		printf(" %c %c%s%c\n", selec[2], selec[2] == '>' ? '<' : ' ', str3, selec[2]);
		printf(" %c  %s \n",   selec[3], str4);
		printf(" %c  %s \n",   selec[4], str5);
		//Input
		chose = getch() - 48;
		//Processing
		switch (chose) {
			case 8: //UP
				selec[row] = ' ';
				row--;
				break;
			case 2: //DOWN
				selec[row] = ' ';
				row++;
				break;
			case 4: //LEFT
				if (row != 0 || row != 4) return(-row);
				break;
			case 6: //RIGHT
				if (row != 0 || row != 4) return( row);
				break;
			case 5: //SET
				if (row == 0 || row == 4) return( 5 * row + 5);
				break;
		}
	}
}

int Convert2Dec(char numb_1[9], int base_1) {
	int numb_dec = 0, len = strlen(numb_1);
	//Z�hler
	int i = 0, j = 0;
	//F�r r�mische Zahlen
	int digit_now = 0, digit_before = 0;
	const char strRom[] = {  'M',  'D',  'C', 'L', 'X', 'V', 'I' };
	const int  strDec[] = { 1000,  500,  100,  50,  10,   5,   1 };
	//Umwandlung
	switch (base_1) {
		case  0:
			//Nur Gro�buchstaben
			for (i = 0; i < len; i++) if (numb_1[i] >= 'a' && numb_1[i] <= 'z') numb_1[i] -= (char)32;
			//Umrechnen (Quelle: http://www.periodni.com/css/roman.js)
			for (i = 0; i < len; i++) {
				//Wertzuweisung
				for (j = 0; j < 7; j++) {
					numb_1[i] == strRom[j] ? digit_now = strDec[j] : digit_now;
				}
				//Ausnahmen wenn vorherige Ziffer kleiner als jetztige ist (z.B. IV = -1 + 5 = 4)
				digit_before < digit_now ? digit_before *= -1 : digit_before;
				//Addition
				numb_dec += digit_before;
				//Verschiebung der vorherigen Ziffer
				digit_before = digit_now;
			}
			//letzte Addition
			numb_dec += digit_before;
			printf("%d", numb_dec);
			system("pause");
			exit(EXIT_SUCCESS);
			break;
		case  1:
			numb_dec = (int)numb_1[0];
			break;
		case  2:
		case  3:
		case  4:
		case  5:
		case  6:
		case  7:
		case  8:
		case  9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
			//Zeichen mit Zahlen ersetzen
			for (i = 0; i < len; i++) {
				if (numb_1[i] >= '0' && numb_1[i] <= '9') numb_1[i] -= (char)48;
				if (numb_1[i] >= 'A' && numb_1[i] <= 'Z') numb_1[i] -= (char)55;
				if (numb_1[i] >= 'a' && numb_1[i] <= 'z') numb_1[i] -= (char)87;
			}
			//Umrechen (Quelle: http://www.arndt-bruenner.de/mathe/scripts/Zahlensysteme.htm#txthorner)
			for (i = 0; i < len; i++) {
				//Hornerschema
				numb_dec = numb_dec * base_1 + numb_1[i];
			}
			break;
	}
	return(numb_dec);
}

char* Convert2Base(int  numb_dec, int base_2) {
	char numb_2[65] = { 0 };
	//Z�hler
	int i = 0, j = 0;
	//Umwandlung
	switch (base_2) {
		case  0:
			break;
		case  1:
			numb_2[0] = (char)(numb_dec % 256);
			numb_2[1] = '\0';
			break;
		case  2:
		case  3:
		case  4:
		case  5:
		case  6:
		case  7:
		case  8:
		case  9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
			break;
	}
	return(numb_2);
}
