#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

char* convert(char number[9], int number_base, int result_base);

int main() {
	int number_base = 0, result_base = 0;
	char number[9] = { 0 }, result[33] = { 0 };
	//Input
		//number
	system("cls");
	printf("   NumberConverter\n");
	printf("   ***************\n\n");
	printf("[Enter a number]\n");
	printf("");
	scanf("%s", number);
		//number_base
	system("cls");
	printf("   NumberConverter\n");
	printf("   ***************\n\n");
	printf("[Enter the base of the number]\n");
	printf("%8s [  ]\b\b\b", number);
	scanf("%d", &number_base);
		//result_base
	system("cls");
	printf("   NumberConverter\n");
	printf("   ***************\n\n");
	printf("[Enter the base of the result]\n");
	printf("%8s [%2d] = [  ]\b\b\b", number, number_base);
	scanf("%d", &result_base);

	//Output
	system("cls");
	printf("   NumberConverter\n");
	printf("   ***************\n\n");
	printf("[processing...]\n");
	printf("%8s [%2d] = [%2d]", number, number_base, result_base);
	system("cls");
	printf("   NumberConverter\n");
	printf("   ***************\n\n");
	printf("[finished]\n");
	printf("%8s [%2d] = [%2d] %s\n\n", number, number_base, result_base, convert(number, number_base, result_base));

	//System Exit
	system("pause");
	main();
	return 0;
}

char* convert(char number[9], int number_base, int result_base) {
	static char result[33] = { 0 };

	/*
	#########################################################
	#                                                       #
	#  number -> number_int -> dec -> result_int -> result  #
	#                                                       #
	#########################################################
	*/

	//Declas
	int number_int[9] = { 0 }, result_int[33] = { 0 }, dec = 0, i = 0, len = strlen(number), helper = 0;
	
	//number -> number_int
	for (i = 0; i < 9; i++) {
		switch (number[i]) {
		case '0':
			number_int[i] = 0;
			break;
		case '1':
			number_int[i] = 1;
			break;
		case '2':
			number_int[i] = 2;
			break;
		case '3':
			number_int[i] = 3;
			break;
		case '4':
			number_int[i] = 4;
			break;
		case '5':
			number_int[i] = 5;
			break;
		case '6':
			number_int[i] = 6;
			break;
		case '7':
			number_int[i] = 7;
			break;
		case '8':
			number_int[i] = 8;
			break;
		case '9':
			number_int[i] = 9;
			break;
		case 'A':
		case 'a':
			number_int[i] = 10;
			break;
		case 'B':
		case 'b':
			number_int[i] = 11;
			break;
		case 'C':
		case 'c':
			number_int[i] = 12;
			break;
		case 'D':
		case 'd':
			number_int[i] = 13;
			break;
		case 'E':
		case 'e':
			number_int[i] = 14;
			break;
		case 'F':
		case 'f':
			number_int[i] = 15;
			break;
		}
	}

	//number_int -> dec
	for (i = 1; i <= len; i++) {
		helper = (int) round(pow(number_base, i - 1));
		dec += number_int[len - i] * helper;
	}

	//dec -> result_int
	for (i = 0; i < 33; i++) {
		helper = (int) dec / round(pow(result_base, i));
		result_int[32 - i] = helper % result_base;
	}

	//result_int -> result
	for (i = 0; i < 33; i++) {
		switch (result_int[i]) {
		case 0:
			result[i] = '0';
			break;
		case 1:
			result[i] = '1';
			break;
		case 2:
			result[i] = '2';
			break;
		case 3:
			result[i] = '3';
			break;
		case 4:
			result[i] = '4';
			break;
		case 5:
			result[i] = '5';
			break;
		case 6:
			result[i] = '6';
			break;
		case 7:
			result[i] = '7';
			break;
		case 8:
			result[i] = '8';
			break;
		case 9:
			result[i] = '9';
			break;
		case 10:
			result[i] = 'A';
			break;
		case 11:
			result[i] = 'B';
			break;
		case 12:
			result[i] = 'C';
			break;
		case 13:
			result[i] = 'D';
			break;
		case 14:
			result[i] = 'E';
			break;
		case 15:
			result[i] = 'F';
			break;
		}
	}

	//Func Exit
	return result;
}