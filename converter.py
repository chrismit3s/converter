#!/usr/bin/env python3

class BaseN():
    alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/" # full base 64 alphabet
    def to_val(cls, str, base):
        """ Converts a string and returns its value as an integer """
        # remove prefixes like "0x" if present
        if(str[0] == "0"):
            if(base ==  2 and str[1] == "b"): # binary prefix
                str = str[2:]
            if(base ==  8 and str[1] == "o"): # octal prefix
                str = str[2:]
            else:
                str = str[1:]
            if(base == 16 and str[1] == "x"): # hexadecimal prefix
                str = str[2:]
        
        # use only capital letters if lowercase is not needed
        if(base <= 36):
            str = str.upper()
        
        # replace "-" with "+" and "_" with "/" (for URL b64 strings)
        if(base >= 63):
            str = str.replace("-", "+")
            str = str.replace("_", "/")

        # check if only valid characters (or spaces) are use, else raise ValueError
        for char in str:
            if(char not in (cls.alphabet[:base] + " ")):
                raise ValueError

        # convert using the horner scheme
        num = 0
        for char in str:
            num = num * base + cls.alphabet.find(char)

        return num
    def to_base(cls, num, base):
        """ Converts an integer to a base and returns it as a string """
        str = ""

        while(num != 0):
            str += cls.alphabet[num % base]
            num //= base
        str = str[::-1]

        return str
class RomanNumerals():
    roman_symbols = { # all roman digits
        "I": 1,
        "V": 5,
        "X": 10,
        "L": 50,
        "C": 100,
        "D": 500,
        "M": 1000
    }
    roman_vals = { # all basic values
        1000: "M",
        900:  "CM",
        500:  "D",
        400:  "CD",
        100:  "C",
        90:   "XC",
        50:   "L",
        40:   "XL",
        10:   "X",
        9:    "IX",
        5:    "V",
        4:    "IV",
        1:    "I"
    }
    def to_val(cls, roman):
        """ Converts a roman numeral and returns its value as an integer """
        # use only capital letters
        roman = roman.upper()

        # check if only valid characters (or spaces) are use, else raise ValueError
        for char in roman:
            if(char not in (list(cls.roman_symbols.keys()) + [" "])):
                raise ValueError

        # get first digit
        prev = cls.roman_symbols.get(roman[0])

        # cycle over all digit (except first)
        res = 0
        for digit in roman[1:]:
            # get its value
            digit = cls.roman_symbols.get(digit)

            # add the previous one, except if the current one is bigger (XI = 10 + 1 = 11; but IX = -1 + 10 = 9)
            if digit > prev:
                prev *= -1
            res += prev
            prev = digit
        
        # add last digit
        res += prev
            
        return res
    def to_roman(cls, value):
        """ Converts an integer to a roman numeral and returns it as a string
        Works only up to 3999 """
        if value > 3999:
            return "MMMCMXCIX"

        res = ""
        while value != 0:
            # find biggest key thats still smaller than value
            for key in cls.roman_vals:
                if key <= value:
                    value -= key
                    res += cls.roman_vals.get(key)
                    break;

        return res


print("Test")
print(1234)
print(RomanNumerals().to_roman(1500))
print(RomanNumerals().to_val("MMMCMXCIX"))